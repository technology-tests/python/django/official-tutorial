# Live demo

[Try the website live][live-demo], generously hosted by MyBinder.
To learn more about MyBinder, view the `README` file in the `.binder` directory of this repository.

## Admin site

The admin site can be accessed under `/admin`.
The default admin account created with fixtures using `./manage.py loaddata` is:

- Username: `admin`
- Password: `123123`


# About

This project loosely follows [the official Django tutorial][django-tutorial].
Several enhancements have been made, including, but not limited to:

- [Parameterized tests][enhancement-parameterized-tests]
- [Fixtures][enhancement-fixtures]
- Display total number of votes for each poll
  ([`question_list.html`][enhancement-total-votes-html], [`views.py`][enhancement-total-votes-view])
- [Redirect from `/` to main polls app][enhancement-redirect-index]
- [Idiomatic code][enhancement-idiom-pagination]
  (use of `paginate_by` instead of [hard-coded Python slicing in tutorial][enhancement-idiom-pagination-tutorial])
- [Better models][enhancement-models]
  (use of `PositiveIntegerField` instead of [`IntegerField` in tutorial][enhancement-models-tutorial])
- [Improved CSS][enhancement-css] ([tutorial's CSS][enhancement-css-tutorial])

For the completed tutorial's code without any deviations,
see [`consideratecode`'s repository][django-tutorial-complete]
(external link, not affiliated).


# Warning

The settings used in this project are not suitable for production.
They have been tweaked to give an optimal experience for the live demo.


[django-tutorial]: https://docs.djangoproject.com/en/4.1/intro/tutorial01/
[django-tutorial-complete]: https://github.com/consideratecode/django-tutorial-step-by-step/tree/2.0
[enhancement-css]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/polls/static/polls/css/main.css
[enhancement-css-tutorial]: https://docs.djangoproject.com/en/4.1/intro/tutorial06/#id1
[enhancement-fixtures]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/fixtures/0001_initial.yaml
[enhancement-idiom-pagination]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/polls/views.py#L15
[enhancement-idiom-pagination-tutorial]: https://docs.djangoproject.com/en/4.1/intro/tutorial04/#id7
[enhancement-models]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/polls/models.py#L32
[enhancement-models-tutorial]: https://docs.djangoproject.com/en/4.1/intro/tutorial02/#id2
[enhancement-parameterized-tests]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/polls/tests.py#L9
[enhancement-redirect-index]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/main/urls.py#L27-31
[enhancement-total-votes-html]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/polls/templates/polls/question_list.html#L39
[enhancement-total-votes-view]: https://gitlab.com/technology-tests/python/django/official-tutorial/-/blob/bbb04289d144a47efcb2695843614967dcf91456/src/polls/views.py#L27
[live-demo]: https://gke.mybinder.org/v2/gl/technology-tests%2Fpython%2Fdjango%2Fofficial-tutorial/HEAD
