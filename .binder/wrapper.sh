#!/usr/bin/env bash

# MyBinder requires the entrypoint to be exec'd
exec python /src/manage.py runserver 0.0.0.0:8888
