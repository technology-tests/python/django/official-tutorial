# Info

## Root directory

- `requirements.txt`:
  requirements for all project components (development, testing, linting, etc).
  Included to have a conventional `requirements.txt` file in the project root

## Current directory

- `requirements-dev.txt`: requirements for all project components
- `requirements-lint.txt`: requirements for linting and formatting
  (for use in GitLab CI pipelines, where running web server is not required)
- `requirements-run.txt`: runtime-only requirements
  (for use in live demo, where development is not required)
- `requirements-test.txt`: requirements for testing
  (for use in GitLab CI pipelines, where running web server is not required)
