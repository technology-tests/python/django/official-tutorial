import bs4


class ProcessHtmlMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if response["content-type"].startswith("text/html;"):
            soup = bs4.BeautifulSoup(response.content, "html.parser")

            formatter = bs4.formatter.HTMLFormatter(indent=2)
            response.content = soup.prettify(formatter=formatter)

        return response
