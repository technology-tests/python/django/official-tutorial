"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView


def make_server_relative_path(path: str) -> str:
    return f"{settings.SERVER_SUBPATH}{path}"


urlpatterns = [
    path(
        make_server_relative_path(""),
        RedirectView.as_view(pattern_name="polls:index", permanent=True),
        name="index",
    ),
    path(make_server_relative_path("admin/"), admin.site.urls),
    path(make_server_relative_path("polls/"), include("polls.urls")),
]
