import os
from unittest import mock

from django.test import SimpleTestCase, override_settings
from django.urls import reverse


class UrlConfTests(SimpleTestCase):
    def test_index_redirect(self):
        self.assertRedirects(
            self.client.get(reverse("index")),
            status_code=301,
            expected_url=reverse("polls:index"),
            fetch_redirect_response=False,
        )
