from django.conf import settings
from django.contrib import admin

admin.site.site_header = "Site administration"
admin.site.index_title = "Index"

# Calling reverse() here results in NoReverseMatch
admin.site.site_url = settings.SERVER_ROOT
