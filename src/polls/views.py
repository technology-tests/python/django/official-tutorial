import os

from django.db.models import F, Sum
from django.db.models.functions import Coalesce
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, ListView

from .models import Choice, Question


class IndexView(ListView):
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["IS_MYBINDER_HOST"] = os.environ.get("IS_MYBINDER_HOST") == "True"

        return context

    def get_queryset(self):
        return (
            Question.objects.filter(pub_date__lte=timezone.now())
            .order_by("-pub_date", "question_text")
            .annotate(total_votes=Coalesce(Sum("choice__votes"), 0))
        )


class QuestionDetailView(DetailView):
    model = Question
    pk_url_kwarg = "question_id"
    template_name_suffix = "_detail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["has_enough_valid_choices"] = (
            len(context["question"].choice_set.all()) > 1
        )

        return context

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultView(DetailView):
    model = Question
    pk_url_kwarg = "question_id"
    template_name_suffix = "_results"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["has_enough_valid_choices"] = (
            len(context["question"].choice_set.all()) > 1
        )

        return context

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)

    if question.pub_date > timezone.now():
        raise Http404("Cannot vote on unreleased questions")

    try:
        selected_choice = question.choice_set.get(pk=request.POST["choice"])
    except (KeyError, Choice.DoesNotExist):
        return render(
            request,
            "polls/question_detail.html",
            {
                "question": question,
                "has_enough_valid_choices": len(question.choice_set.all()) > 1,
                "error_message": "Please select a valid choice.",
            },
        )
    else:
        selected_choice.votes = F("votes") + 1
        selected_choice.save()

        return HttpResponseRedirect(
            reverse("polls:results", kwargs={"question_id": question.id})
        )
