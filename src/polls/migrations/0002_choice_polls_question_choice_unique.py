from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("polls", "0001_initial"),
    ]

    operations = [
        migrations.AddConstraint(
            model_name="choice",
            constraint=models.UniqueConstraint(
                fields=("question", "choice_text"), name="polls_question_choice_unique"
            ),
        ),
    ]
