from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    # ListView page
    list_display = ("question_text", "pub_date", "was_published_recently")
    list_filter = ["pub_date"]
    search_fields = ["question_text", "choice__choice_text"]

    # DetailView page
    inlines = [ChoiceInline]


admin.site.register(Question, QuestionAdmin)
