import datetime
import os
from unittest import mock

from bs4 import BeautifulSoup
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from parameterized import parameterized, parameterized_class

from .models import Choice, Question

time_offset_type = {
    "past": datetime.timedelta(days=-1),
    "recent": datetime.timedelta(hours=-1),
    "unreleased": datetime.timedelta(hours=1),
}


def create_question(question_text, time_offset):
    pub_date = timezone.now() + time_offset
    question = Question.objects.create(question_text=question_text, pub_date=pub_date)

    return question


def create_choice(question, choice_text, votes=0):
    choice = Choice.objects.create(
        question=question, choice_text=choice_text, votes=votes
    )

    return choice


class QuestionModelTests(TestCase):
    @parameterized.expand(
        [
            ("Past question", time_offset_type["past"], False),
            ("Recent question", time_offset_type["recent"], True),
            ("Unreleased question", time_offset_type["unreleased"], False),
        ]
    )
    def test_was_published_recently(self, name, time_offset, expected):
        question = create_question(name, time_offset)

        self.assertIs(question.was_published_recently(), expected)


class QuestionIndexViewTests(TestCase):
    context_var = "question_list"
    route = "polls:index"

    def test_no_questions(self):
        response = self.client.get(reverse(self.route))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context[self.context_var], [])

    @parameterized.expand(
        [
            (
                "Past questions only",
                {
                    "past": 1,
                    "recent": 0,
                    "unreleased": 0,
                },
            ),
            (
                "Recent questions only",
                {
                    "past": 0,
                    "recent": 1,
                    "unreleased": 0,
                },
            ),
            (
                "Unreleased questions only",
                {
                    "past": 0,
                    "recent": 0,
                    "unreleased": 1,
                },
            ),
            (
                "All question types",
                {
                    "past": 1,
                    "recent": 1,
                    "unreleased": 1,
                },
            ),
            (
                "Truncated questions list",
                {
                    "past": 1,
                    "recent": 10,
                    "unreleased": 1,
                },
            ),
        ]
    )
    def test_listed_questions(self, name, num_questions):
        questions = {}

        for question_type in time_offset_type.keys():
            questions[question_type] = [
                create_question(
                    f"{question_type} {i:03d}",
                    # Most recently published should be in the beginning of the list
                    time_offset_type[question_type] + datetime.timedelta(seconds=-i),
                )
                for i in range(num_questions[question_type])
            ]

        response = self.client.get(reverse(self.route))

        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context[self.context_var],
            # Only the n most recently published should be listed
            (questions["recent"] + questions["past"])[:5],
        )

    @parameterized.expand(
        [
            ("No questions", 0, 1),
            ("One question", 1, 1),
            ("Four questions", 4, 1),
            ("Five questions", 5, 1),
            ("Six questions", 6, 1),
            ("Six questions", 6, 2),
        ]
    )
    def test_pagination(self, name, num_questions, current_page_num):
        paginate_by = 5

        if num_questions == 0:
            num_pages = 1
        else:
            num_pages = ((num_questions - 1) // paginate_by) + 1

        for i in range(num_questions):
            create_question(f"question {i}", time_offset=time_offset_type["past"])

        url = reverse(self.route)
        response = self.client.get(f"{url}?page={current_page_num}")

        self.assertContains(response, f"Page {current_page_num} of {num_pages}")


@parameterized_class(
    ("route"),
    [
        ("polls:detail",),
        ("polls:results",),
        ("polls:vote",),
    ],
)
class DetailViewTests(TestCase):
    @parameterized.expand(["past", "recent"])
    def test_released_question(self, question_type):
        question = create_question(question_type, time_offset_type[question_type])

        url = reverse(self.route, kwargs={"question_id": question.id})
        response = self.client.get(url)

        self.assertContains(response, question.question_text)

    def test_unreleased_question(self):
        question = create_question("unreleased", time_offset_type["unreleased"])

        url = reverse(self.route, kwargs={"question_id": question.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)


class QuestionDetailViewTests(TestCase):
    route = "polls:detail"

    @parameterized.expand([("past", 0), ("past", 1)])
    def test_not_contains_enough_choices(self, question_type, num_choices):
        question = create_question(question_type, time_offset_type[question_type])

        for i in range(num_choices):
            create_choice(question, f"choice {i}")

        url = reverse(self.route, kwargs={"question_id": question.id})
        response = self.client.get(url)

        self.assertContains(response, question.question_text)
        self.assertContains(response, "Not enough valid choices")

        soup = BeautifulSoup(response.content, "html.parser")
        element = soup.find("button", {"id": "submit"})
        self.assertIsNone(element)

    @parameterized.expand([("past", 2), ("past", 100)])
    def test_contains_enough_choices(self, question_type, num_choices):
        question = create_question(question_type, time_offset_type[question_type])

        choices = []

        for i in range(num_choices):
            choice = create_choice(question, f"choice {i}")
            choices.append(choice)

        url = reverse(self.route, kwargs={"question_id": question.id})
        response = self.client.get(url)

        self.assertContains(response, question.question_text)

        for choice in choices:
            self.assertContains(response, choice.choice_text)

        soup = BeautifulSoup(response.content, "html.parser")
        element = soup.find("button", {"id": "submit"})
        self.assertIsNotNone(element)


class VoteViewTests(TestCase):
    route = "polls:vote"

    @parameterized.expand(["past", "recent"])
    def test_released_question(self, question_type):
        question = create_question(question_type, time_offset_type[question_type])
        choice = create_choice(question, "choice", votes=0)

        url = reverse(self.route, kwargs={"question_id": question.id})
        data = {"choice": choice.id}
        response = self.client.post(url, data)

        self.assertRedirects(
            response,
            reverse("polls:results", kwargs={"question_id": question.id}),
            fetch_redirect_response=False,
        )

        choice.refresh_from_db()

        self.assertEqual(choice.votes, 1)

    @parameterized.expand(["past", "recent"])
    def test_vote_invalid_choice(self, question_type):
        question = create_question(question_type, time_offset_type[question_type])

        for i in range(3):
            create_choice(question, f"choice {i}")

        url = reverse(self.route, kwargs={"question_id": question.id})
        data = {"choice": 99999}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Please select a valid choice.")


@mock.patch.dict(os.environ, {"IS_MYBINDER_HOST": "True"}, clear=True)
class EnvironmentMyBinderTests(TestCase):
    def test_index_warning(self):
        response = self.client.get(reverse("polls:index"))

        self.assertContains(response, "Warning: this site appears to be hosted by")


@mock.patch.dict(os.environ, {}, clear=True)
class EnvironmentDefaultTests(TestCase):
    def test_index_warning(self):
        response = self.client.get(reverse("polls:index"))

        self.assertNotContains(response, "Warning: this site appears to be hosted by")
